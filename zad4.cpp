#include <iostream>

using namespace std;

bool jestPal(string testStr);

int main()
{
    string wyraz;

    while(true)
    {
        cout<<"Program sprawdzający czy podany wyraz jest palindromem"<<endl;
        cout << endl<<"Podaj wyraz do sprawdzenia, albo \"0\" zeby wyjsc:" << endl<<endl;
        cin >>wyraz;
        if(wyraz=="0") break;

        if(jestPal(wyraz)) cout<<"Podany wyraz jest palindromem"<<endl<<endl;
        else cout<<"Podany wyraz nie jest palindromem"<<endl<<endl;
    }

    return 0;
}

bool jestPal(string testStr)
{
    int i=0; //zmienna indeksu pierwszej litery badanego słowa
    int j=testStr.length()-1; //zmienna indeksu ostatniej litery badanego słowa
    //szczególny przypadek
    //jeżeli słowo zawiera dwa takie same znaki lub jest tylko jednym znakiem to słowo jest palindromem
    if(((j-i == 1) || (i==j)) && (testStr[i] == testStr[j])) return true;
    //funkcja rekurencyjna porównująca przeciwległe litery słowa
    else if(testStr[i]==testStr[j])
    {
        //funkcja rekurencyjnie bada wyraz pomniejszony o pierwszą i ostatnią literę niezależnie od długości wyrazu
        //wyraz.substr zwraca sring rozpoczynający się od litery z indeksem 1 oraz o długości pomniejszonej o 2
        //(ponieważ indeksowanie rozpoczyna się od 0)
        return jestPal(testStr.substr(1,(testStr.length()-2)));
    }
    //jeżeli którekolwiek przeciwległe litery nie są takie same to funkcja zwraca fałsz
    else return false;

}
