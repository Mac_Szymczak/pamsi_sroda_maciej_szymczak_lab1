#include <iostream>
#include <ctime>
#include <cstdlib>
#include <fstream>

using namespace std;

int *stworz(int n);
void wypisz(int *tab, int n);
void ztxt(int *tab, int n);
void wtxt(int *&tab, int &n);
void zbin(int *tab, int n);
void wbin(int *&tab, int &n);


int main ()
{
    int opcja;
    int n;
    int *tab=NULL;
    srand(time(NULL));
    do
    {
        cout << "1.Stworz tablice"<<endl;
        cout << "2.Wypisz tablice"<<endl;
        cout << "3.Zapisz do pliku"<<endl;
        cout << "4.Wczytaj z pliku"<<endl;
        cout << "5.Zapisz do pliku bin"<<endl;
        cout << "6.Wczytaj z pliku bin"<<endl;
        cout << "0.Koniec"<<endl;
        cout << "Wybierz opcje"<<endl;
        cin >> opcja;
        switch(opcja)
        {
        case 0:
            break;
        case 1:
        {
            cout<<"Liczba znakow: ";
            cin>>n;

            tab=stworz(n);

            break;
        }
        case 2:
        {
            wypisz(tab,n);

            break;
        }
        case 3:
        {
            ztxt(tab,n);
            break;
        }
        case 4:
        {
            wtxt(tab,n);
            break;
        }
        case 5:
        {
            zbin(tab,n);
            break;
        }
        case 6:
        {
            wbin(tab,n);
            break;
        }
        }
    }
    while(opcja !=0);



//zniszcz tab2
//for ( int i(0); i < n; ++i )
//delete [] tab[i]; //uwolnienie pamieci
//delete [] tab; //uwolnienie pamieci
//tab = NULL;

    return 0;
}

//funkcja tworzy tablicê jednowymiarow¹ o zadanej wielkoœci wype³nion¹ liczbami od 0 do 100
int *stworz(int n)
{
    int * tab = new int [n]; //alokacja pamieci na tablicê jednowymiarow¹
    for ( int i = 0; i < n; i++ )
    {
        tab[i] = rand()%100+1;
    }
    return tab;
}

void wypisz(int *tab, int n)
{
    for ( int i = 0; i < n; i++, cout<<endl )
        cout<<tab[i]<<'\t';

}

//funkcja do zapisu do pliku txt
void ztxt(int *tab, int n)
{
    fstream plik;//deklaracja zmiennej typu fstream
    plik.open("tabela.txt", ios::out );//otworzenie pliku z zadan¹ nazw¹ i odwo³anie siê do zapisu(ios::out)
    if(plik.good()==true)
    {
        cout<<"Plik utworzony"<<endl;
        for(int i=0; i<n; i++)
        {
            plik <<tab[i]<<" ";
        }
    }
    else cout<<"blad"<<endl;
}

void wtxt(int *&tab, int &n)
{
    fstream plik;
    plik.open("tabela.txt",ios::in );//otworzenie pliku z zadan¹ nazw¹ i odwo³anie siê do odczytu(ios::in)
    if(plik.good()==true)
    {
        cout<<"plik otwarty"<<endl;
        int *tab_tmp=NULL; //zadeklarowanie wskaŸnika na tymczasow¹ tablicê
        int tmp;//deklaracja zmiennej tymczasowej
        n=0;//ustalenie pocz¹tkowego rozmiaru =0
        while(!((plik>>tmp).eof()))//jednoczeœnie do zmiennej tmp zapisuje siê liczba z pliku i sprawdza czy nie ma kolejnych
        {
            if(cin.eof())//jeŸeli pojawi siê koniec pliku pêtla siê przerwie
            {
                cout<<"Koniec"<<endl;
                break;
            }
            n++;//kolejno zwiêkszany jest rozmiar tablic a¿ do osi¹gniêcia kojca pliku
            tab_tmp=new int [n];//alokacja pamiêci na tymczasow¹ tablicê
            //pêtla przypisuje do tymczasowej tablicy obecn¹ tablicê i zostaje miejsce na jeszcze jedn¹ liczbê
            for(int i=0; i<(n-1); i++)
            {
                tab_tmp[i]=tab[i];
            }
            tab_tmp[n-1]=tmp; //dopisanie tej ostatniej cyfry do tablicy tymczasowej z pliku

            delete[]tab; //usuniêcie obecnej tablicy (dane s¹ zapamiêtane w tymczasowej)
            tab=new int [n];//alokacja pamiêci na now¹ tablicê
            //pêtla uzupe³nia now¹ tablicê danymi z tymczasowej
            for(int i=0; i<n; i++)
            {
                tab[i]=tab_tmp[i];
            }
            delete [] tab_tmp;//usuniêcie tymczasowej tablicy
        }
        plik.close();
    }
}
//poni¿ej funkcje s³u¿¹ do zapisu i odczytu z plikow binarnych i s¹ niemal identyczne jak powy¿sze z jedn¹ ró¿nic¹ przy otwieraniu pliku
void zbin(int *tab, int n)
{
    fstream plik;
    plik.open("tabela.bin", ios::out | ios::binary);//podczas otwierania pliku nale¿y zaznaczyæ ¿e dane bêd¹ dotyczyæ plików binarnych
    if(plik.good()==true)
    {
        cout<<"Plik utworzony"<<endl;
        for(int i=0; i<n; i++)
        {
            plik << tab[i] << " ";
        }
    }
    else cout<<"blad"<<endl;
}

void wbin(int *&tab, int &n)
{
    fstream plik;
    plik.open("tabela.bin",ios::in| ios::binary );//podczas otwierania pliku nale¿y zaznaczyæ ¿e dane bêd¹ dotyczyæ plików binarnych
    if(plik.good()==true)
    {
        cout<<"plik otwarty"<<endl;
        int *tab_tmp=NULL;
        int tmp;
        n=0;
        while(!((plik>>tmp).eof()))
        {
            if(cin.eof())
            {
                cout<<"Koniec"<<endl;
                break;
            }
            n++;
            tab_tmp=new int [n];
            for(int i=0; i<(n-1); i++)
            {
                tab_tmp[i]=tab[i];
            }
            tab_tmp[n-1]=tmp;

            delete[]tab;
            tab=new int [n];
            for(int i=0; i<n; i++)
            {
                tab[i]=tab_tmp[i];
            }
            delete [] tab_tmp;
        }
        plik.close();

    }
}
