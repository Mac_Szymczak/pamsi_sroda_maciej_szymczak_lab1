#include <iostream>

using namespace std;

int Potega(int x, int p);
int Silnia(int x);

int main()
{
    int  opcja;
    do
    {
        cout << "1.Potegowanie"<<endl;
        cout << "2.Silnia"<<endl;

        cout << "0.Koniec"<<endl;
        cout << "Wybierz opcje" <<endl<<endl;
        cin >> opcja;
        switch(opcja)
        {
        case 0:


            break;
        case 1:
        {
            int x=0;
            int p=0;

            cout<<"Podaj podstawe:"<<endl;
            cin>>x;
            cout<<"Podaj wykladnik:"<<endl;
            cin>>p;

            cout << x<<"^"<<p<<" = "<<Potega(x,p)<<endl;
            break;
        }
        case 2:
        {
            int x=0;
            cout<<"Podaj liczbe:"<<endl;
            cin>>x;
            cout<<x<<"! = "<<Silnia(x)<<endl;
            break;
        }
        }
    }
    while(opcja!=0);
}
//funkcja potêguj¹ca rekurencyjnie
int Potega(int x, int p)
{
    //przypadek podstawowy funkcji
    if(p==0) return 1;//je¿eli wyk³adnik jest równy 0 funkcja zwraca 1
    //przypadek z³o¿ony funkcji
    else return x*Potega(x,(--p));//mno¿enie zadanej podstawy przez wartoœæ funkcji o wyk³adniku pomniejszonym o 1
}

//funkcja obliczaj¹ca silnie liczby rekurencyjnie
int Silnia(int x)
{
    //przypadek podstawowy funkcji
    if(x==0) return 1;//silnia 0 jest równa 1
    else return x*Silnia(x-1);//mno¿enie zadanej liczby przez wartoœæ funkcji dla poprzednich liczb
}
