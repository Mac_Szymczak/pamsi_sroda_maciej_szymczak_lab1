#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

//Deklaracje u¿ytych funkcji
int **stworz(int n, int m);
void wypelnij(int **tab, int maksimum, int n, int m);
void wypisz(int **tab, int n, int m);
void znajdz(int **tab, int n, int m);

int main ()
{
    int opcja;//zmienna potrzebna do menu
    int n,m;  // zmienne odpowiadaj¹ce za iloœæ wierszy (n) i iloœæ kolumn (m)
    int **tab=NULL;
    srand(time(NULL)); //funkcja odpowiada z poprawny wybór liczb losowych
    do
    {
        cout << "1.Stworz tablice"<<endl;
        cout << "2.Wypelnij tablice"<<endl;
        cout << "3.Wypisz tablice"<<endl;
        cout << "4.Znajdz maksimum"<<endl;
        cout << "0.Koniec"<<endl;
        cout << "Wybierz opcje"<<endl;
        cin >> opcja;
        switch(opcja)
        {
        case 0:
            break;
        case 1:
        {
            cout<<"Liczba wierszy: ";
            cin>>n;
            cout<<"Liczba kolumn: ";
            cin>>m;

            tab=stworz(n,m);

            break;
        }
        case 2:
        {
            int maks;
            cout<<"Podaj maksymalny zakres:";
            cin>>maks;

            wypelnij(tab,maks,n,m);
            break;
        }
        case 3:
        {
            wypisz(tab,n,m);

            break;
        }
        case 4:
        {
            znajdz(tab,n,m);
            break;
        }
        }
    }
    while(opcja !=0);

//for ( int i(0); i < n; ++i )
//delete [] tab[i]; //uwolnienie pamieci
//delete [] tab; //uwolnienie pamieci
//tab = NULL;

    return 0;
}

//funkcja tworzenia w pamiêcidynamiecznej tablicy dwuwymiarowej
int **stworz(int n, int m)
{
    int ** tab = new int * [n]; //alokacja pamieci na wiersze
    for ( int i = 0; i < n; i++ )
    {
        tab[i] = new int [m]; //alokacja pamieci na kolumny
    }
    return tab;
}

void wypelnij(int **tab, int maks, int n, int m)
{
    //Petla przypisuj¹ca do tablicy dwuwymiarowej liczby ca³kowite od 0 do ustalonego maksimum.
    for ( int i = 0; i < n; i++ )
    {
        for ( int j = 0; j < m; j++)
        {
            tab[i][j]=rand()%(maks+1);
        }
    }
}

void wypisz(int **tab, int n, int m)
{
    for ( int i = 0; i < n; i++, cout<<endl )
        for ( int j = 0; j < m; j++)
            cout<<tab[i][j]<<'\t';

}

void znajdz(int **tab, int n, int m)
{
    int x=tab[0][0];//pocz¹tkowo maksimum to pierwszy element tablicy

    //pêtla przeszukuje wszystkie elementy tablicy i porównuje je do aktualnego maksimum
    //je¿eli znajdzie wiêksz¹ liczbê to zastêpuje ni¹ obecne maksimum
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++)
        {
            if(tab[i][j]>x)
            {
                x=tab[i][j];
            }
        }
    }
    cout<<endl<<"Maksimum: "<<x<<endl;
}
